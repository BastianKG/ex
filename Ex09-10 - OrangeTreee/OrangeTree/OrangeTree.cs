﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrangeTreeSim
{
    public class OrangeTree
    {
        private int age;
        private int height;
        private bool treeAlive;
        private int numOranges;
        private int orangesEaten;

        public void SetAge(int age)
        {
            this.age = age;
        }

        public int GetAge()
        {
            return age;
        }
        public void SetHeight(int height)
        {
            this.height = height;
        }
        
        public int GetHeight()
        {
            return height;
        }
        public void SetTreeAlive(bool treeAlive)
        {
            this.treeAlive = treeAlive;
        }
        public bool GetTreeAlive()
        {
            return treeAlive;
        }

        public int GetNumOranges()
        {
            return numOranges;
        }
        public int GetOrangesEaten()
        {
            return orangesEaten;
        }
        public void OneYearPasses()
        {
            age++;
            orangesEaten = 0;

            if (age < 80)
            {
                height += 2;
                SetTreeAlive(true);
                numOranges = 5 * (age-1);
            }
            else
            {
                SetTreeAlive(false);
            }
        }

        public void EatOrange(int count)
        {
            if (GetNumOranges() >= count)
            {
                numOranges = GetNumOranges() - count;
                orangesEaten += count;
            }
            else
            {
                Console.WriteLine("You don't have enough oranges to eat that many, dummy!");
            }
        }

    }
}
