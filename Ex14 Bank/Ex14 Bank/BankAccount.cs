﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ex14_Bank
{
    public class BankAccount
    {
        public string Name { get; set; }

        private double balance;

        public double Balance
        {
            get { return balance; }
        }

        private bool locked;


        public BankAccount(double balance)
        {
            this.balance = balance;
        }
        public BankAccount(string name, double balance) : this(balance)
        {
            Name = name;
        }
        public BankAccount(string name, double balance, bool locked) : this(name, balance)
        {
            this.locked = locked;
        }

        public void Deposit(double amount)
        {
            if (locked == false)
                balance += amount;
            else
                Console.WriteLine("Your account is locked");
        }
        public void Withdraw(double amount)
        {
            if (amount <= balance)
            {
                if (locked == false)
                    balance -= amount;
                else
                    Console.WriteLine("Your account is locked");
            }
            else
                Console.WriteLine("You balance is too low to withdraw the requested amount");
        }

        public void ChangeLockState()
        {
            if (locked == true)
                locked = false;
            else
                locked = true;
        }

        public override string ToString()
        {
            return $"Name: {Name}, Balance: {Balance}";
        }

    }
}
