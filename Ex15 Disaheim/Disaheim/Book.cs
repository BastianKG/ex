﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Disaheim
{
   public class Book : Merchandise
    {

        public string Title { set; get; }

        public double Price { set; get; }



        public Book(string ItemId)// : base(ItemId)
        {
            this.ItemId = ItemId;
        }
        public Book(string ItemId, string Title) : this(ItemId)
        {
            this.Title = Title;
        }
        public Book(string ItemId, string Title, double Price) : this(ItemId, Title)
        {
            this.Price = Price;
        }
        public override string ToString()
        {
            return $"ItemId: {ItemId}, Title: {Title}, Price: {Price}";
        }
    }
}
