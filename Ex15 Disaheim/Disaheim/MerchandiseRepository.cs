﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disaheim
{
    public class MerchandiseRepository
    {
        private List<Merchandise> merchandiseList = new();

        public void AddMerchandise(Merchandise merchandise)
        {
            merchandiseList.Add(merchandise);
        }

        public Merchandise GetMerchandise(string itemId)
        {
            Merchandise merchandise = merchandiseList.Find(merchandise => merchandise.ItemId == itemId);
            return merchandise;
        }

        public double GetTotalValue()
        {
            double totalValue = 0;
            foreach (var merchandise in merchandiseList)
            {
                
            }
            return totalValue;
        }
    }
}
