﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Disaheim
{
   public class Amulet : Merchandise
    {

        public string Design { get; set; }
        public Level Quality { get; set; }



        public Amulet(string ItemId)// : base(ItemId)
        {
            this.ItemId = ItemId;
        }
        public Amulet(string ItemId, Level Quality) : this(ItemId)
        {
            //from derived class
            this.Quality = Quality;
        }
        public Amulet(string ItemId, Level Quality, string Design) : this(ItemId, Quality)
        {
            this.Design = Design;
        }
        public override string ToString()
        {
            return $"ItemId: {ItemId}, Quality: {Quality}, Design: {Design}";
        }
    }
}
