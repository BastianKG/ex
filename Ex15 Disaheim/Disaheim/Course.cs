﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Disaheim
{
    public class Course
    {
        public string Name { set; get; }
        public int DurationInMinutes { set; get; }

        public Course(string name)
        {
            Name = name;
        }

        public Course(string name, int durationInMinutes) : this(name)
        {
            DurationInMinutes = durationInMinutes;
        }

        public override string ToString()
        {
            return $"Name: {Name}, Duration in Minutes: {DurationInMinutes}";
        }
    }
}
