﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disaheim
{
    public class CourseRepository
    {
        private List<Course> courses = new();

        public void AddCourse(Course course)
        {
            courses.Add(course);
        }

        public Course GetCourse(string name)
        {
            Course course = courses.Find(course => course.Name == name);
            return course;
        }

        public double GetTotalValue()
        {
            double totalValue = 0;
            foreach (var course in courses)
            {
                Utility utility = new Utility();
                totalValue += utility.GetValueOfCourse(course);
            }
            return totalValue;
        }
    }
}
