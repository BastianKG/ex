﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Disaheim
{
    public class Utility
    {
        public double GetValueOfBook(Book book)
        {
            return book.Price;
            
        }

        public double GetValueOfAmulet(Amulet amulet)
        {
            double worth = 0;
            if (amulet.Quality == Level.low)
            {
                worth = 12.5;
            }
            else if (amulet.Quality == Level.medium)
            {
                worth = 20;
            }
            else if (amulet.Quality == Level.high)
            {
                worth = 27.5;
            }
            
            return worth;

        }

        public double GetValueOfMerchandise(Merchandise merchandise)
        {
            double worth = 0;
            
            if (merchandise.Quality == Level.low)
            {
                worth = 12.5;
            }
            else if (amulet.Quality == Level.medium)
            {
                worth = 20;
            }
            else if (amulet.Quality == Level.high)
            {
                worth = 27.5;
            }
            return worth;
        }

        public double GetValueOfCourse(Course course)
        {
            int startedHours = (course.DurationInMinutes + 59) / 60;
            double value = startedHours * 875;
            return value;
        }
    }
}
