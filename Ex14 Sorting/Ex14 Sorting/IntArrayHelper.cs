﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ex14_Sorting
{
    public class IntArrayHelper
    {
        public void Sort(int[] intArray)
        {
            int temp;
            for (int i = 0; i <= intArray.Length - 1; i++)
            {
                for (int j = i + 1; j < intArray.Length; j++)
                {
                    if (intArray[i] > intArray[j])
                    {
                        temp = intArray[i];
                        intArray[i] = intArray[j];
                        intArray[j] = temp;
                    }
                }
            }
        }

        public void Reverse(int[] intArray)
        {
            int temp;
            for (int i = 0; i <= intArray.Length-1; i++)
            {
                for (int j = i+1; j < intArray.Length; j++)
                {
                    if(intArray[i] < intArray[j])
                    {
                        temp = intArray[i];
                        intArray[i] = intArray[j];
                        intArray[j] = temp;
                    }
                }
            }
        }
    }
}
