﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using WPFAndMVVM2.Models;
using System.ComponentModel;

namespace WPFAndMVVM2.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private PersonRepository personRepo = new PersonRepository();
        public ObservableCollection<PersonViewModel> PersonsVM { get; set; }
        private PersonViewModel selectedPerson;
        public PersonViewModel SelectedPerson
        {
            get { return selectedPerson; }
            set { selectedPerson = value; OnPropertyChanged("SelectedPerson"); }
        }


        // Implement the rest of this MainViewModel class below to 
        // establish the foundation for data binding !
        public MainViewModel()
        {
            List<Person> persons = personRepo.GetAll();
            PersonsVM = new ObservableCollection<PersonViewModel>();
            foreach (Person person in persons)
            {
                PersonsVM.Add(new PersonViewModel(person));
            }

        }

        public void AddDefaultPerson()
        {
            personRepo.Add("Specify FirstName", "Specify LastName", 0, "Specify Phone");
            List<Person> persons = personRepo.GetAll();
            PersonsVM.Add(new PersonViewModel(persons[persons.Count - 1]));
            SelectedPerson = PersonsVM[PersonsVM.Count - 1];
        }

        public void DeleteSelectedPerson()
        {
            SelectedPerson.DeletePerson(personRepo);
            PersonsVM.Remove(SelectedPerson);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
