﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Persistens
{
    public class Person
    {
        private string name;
        public string Name
        {
            get { return name; }
            set 
            { 
                if (value.Length < 1 || value == null)
                {
                    name = "Invalid name";
                    throw new Exception("A name must be at least 1 character long");

                }
                else
                    name = value;

            }
        }

        private DateTime birthDate;
        public  DateTime BirthDate
        {
            get { return birthDate; }
            set 
            {
                if (value.CompareTo(new DateTime(1900, 1, 1)) == -1 || value.CompareTo(new DateTime(1900, 1, 1)) == 0)
                {
                    birthDate = new DateTime(1900, 1, 1);
                    throw new Exception("Date must be after 1st of January, 1900");

                }
                birthDate = value; 
            }
        }

        private double height;
        public double Height
        {
            get { return height; }
            set 
            {
                {
                    if (value < 1 )
                    {
                        height = 1;
                        throw new Exception("Height must be at least above 1");

                    }
                    else
                        height = value;

                }
            }
        }

        private bool isMarried;
        public bool IsMarried
        {
            get { return isMarried; }
            set { isMarried = value; }
        }

        private int noOfChildren;
        public int NoOfChildren
        {
            get { return noOfChildren; }
            set 
            {
                if (value < 0)
                {
                    noOfChildren = 1;
                    throw new Exception("Number of children must be at least 1");

                }
                else
                    noOfChildren = value;
            }
        }

        public Person(string name, DateTime birthDate, double height, bool isMarried, int noOfChildren)
        {
            Name = name;
            BirthDate = birthDate;
            Height = height;
            IsMarried = isMarried;
            NoOfChildren = noOfChildren;
        }
        public Person(string name, DateTime birthDate, double height, bool isMarried)
        {
            Name = name;
            BirthDate = birthDate;
            Height = height;
            IsMarried = isMarried;
            NoOfChildren = 0;
        }



        public string MakeTitle()
        {
            string title = name + ";" + birthDate + ";" + height + ";" + isMarried + ";" + noOfChildren;
            return title;
        }
    }
}
