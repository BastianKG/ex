﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace Persistens
{
    public class DataHandler
    {
        private string dataFileName;
        public string DataFileName
        {
            get { return dataFileName; }
        }

        public DataHandler(string dataFileName)
        {
            this.dataFileName = dataFileName;
        }

        public void SavePerson(Person person)
        {
            StreamWriter writer = new StreamWriter(dataFileName);
            writer.WriteLine(person.MakeTitle());
            writer.Close();
        }
        public Person LoadPerson()
        {
            StreamReader reader = new StreamReader(dataFileName);
            string fileInput = reader.ReadLine();

            string[] personStrings = fileInput.Split(';');

            string name = personStrings[0];
            DateTime birthDate = DateTime.Parse(personStrings[1]);
            double height = double.Parse(personStrings[2]);
            bool isMarried = bool.Parse(personStrings[3]);
            int noOfChildren = int.Parse(personStrings[4]);


            Person person = new Person(name, birthDate, height, isMarried, noOfChildren);
            reader.Close();
            return person;       
        }
        public void SavePersons(Person[] persons)
        {
            StreamWriter writer = new StreamWriter(dataFileName);
            for (int i = 0; i < persons.Length; i++)
            {
                writer.WriteLine(persons[i].MakeTitle());
            }
            writer.Close();
        }
        public Person[] LoadPersons()
        {
            Person[] persons = new Person[20];
            string fileInput;
            int i = 0;

            StreamReader reader = new StreamReader(dataFileName);
            while ((fileInput = reader.ReadLine()) != null)
            {
                string[] personStrings = fileInput.Split(';');

                string name = personStrings[0];
                DateTime birthDate = DateTime.Parse(personStrings[1]);
                double height = double.Parse(personStrings[2]);
                bool isMarried = bool.Parse(personStrings[3]);
                int noOfChildren = int.Parse(personStrings[4]);

                Person person = new Person(name, birthDate, height, isMarried, noOfChildren);
                persons[i] = person;
                i++;
            }
            reader.Close();
            return persons;
        }
    }
}
