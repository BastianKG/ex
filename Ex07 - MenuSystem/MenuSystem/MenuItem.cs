﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MenuSystem
{
    public class MenuItem
    {
        public string Title;

        public MenuItem (string title)
        {
            Title = title;
        }
    }
}
