﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MenuSystem
{
    public class Menu
    {
        public string Title;

        private MenuItem[] menuItems = new MenuItem [10];

        int itemCount = 0;

        public Menu(string title)
        {
            Title = title;
        }

        public void Show()
        {
            Console.Clear();
            Console.WriteLine(Title + "\n");
            for (int i = 0; i < itemCount; i++)
            {
                Console.WriteLine("  " + menuItems[i].Title);
            }
            Console.WriteLine("\n(Tryk menupunkt eller 0 for at afslutte)");

        }
        public void AddMenuItem(string menuTitle)
        {
            menuItems[itemCount] = new MenuItem(menuTitle);
            itemCount++;
        }

        public int SelectMenuItem()
        {
            string selectionString;
            int selectionInt = -1;
            bool isValid = false;
            while(!isValid)
            {
                selectionString = Console.ReadLine();
                try
                {
                    selectionInt = int.Parse(selectionString);
                    if(selectionInt >= 0 && selectionInt <= itemCount)
                    {
                        isValid = true;
                    }
                    if (isValid)
                    {
                        return selectionInt;
                    }
                    else
                    {
                        Console.WriteLine("Input was invalid. Try again");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
            return selectionInt;
        }
        public void ShowMenuItem(int x)
        {
            switch (x)
            {
                case 0:
                    Console.WriteLine("Goodbye. Peace out!");
                    break;
                case 1:
                    Console.WriteLine("asd1");
                    break;
                case 2:
                    Console.WriteLine("dsfdsfg2");
                    break;
                case 3:
                    Console.WriteLine("gfhj3");
                    break;
                case 4:
                    Console.WriteLine("asdsag4");
                    break;

                default:
                    Console.WriteLine("Something went terribly wrong");
                    break;
            }
        }


    }
}
