﻿using System;

namespace MenuSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu mainMenu = new Menu("Min fantastiske menu");
            // First menu item
            mainMenu.AddMenuItem("1. Gør dit");
            mainMenu.AddMenuItem("2. Gør dat");
            mainMenu.AddMenuItem("3. Gør noget");
            mainMenu.AddMenuItem("4. Få svaret på livet, universet og alting");
                        
            mainMenu.Show();

            int selection = mainMenu.SelectMenuItem();

            mainMenu.ShowMenuItem(selection);
            Console.ReadLine();
        }

    }
}
